//import { remote } from 'electron';
import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { render } from 'react-dom';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import { App } from './app';

const useStyles = makeStyles({
  list: {
    width: window.innerWidth-20,
  },
  fullList: {
    width: 'auto',
  },
});

export function SliderDrawer() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  type DrawerSide = 'top' | 'left' | 'bottom' | 'right';
  const toggleDrawer  = (side: DrawerSide, open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  var pathStr = "";

 const setPath = (selectedPathStr) =>  {
    pathStr = selectedPathStr;

 }

  //let app: App;


  const setAppRef=(  app:App ) => {
    //app = a;
    //pathStr = path;

    //setState( editorTheme: themeManager.getEditorTheme())
      if (app ) {
       //console.log("pathStr in setAppref:test =" + pathStr);
       app.openOpenRepo(pathStr);


     }

  }






  const sideList = (side: DrawerSide) => (
  <div
      className={classes.list}
      role="presentation"
      //onClick={toggleDrawer(side, false)}
      //onKeyDown={toggleDrawer(side, false)}
    style={{backgroundColor:'#333333', height:'100%',color:'#FFFFFF'}} >
    <button onClick={toggleDrawer('right', false)} style={{color:'white'}}> close drawer</button>

    <App ref={setAppRef} />

    </div>
  );

  return (

    <div >
         <p>
         <button id="ray1" style={{color:'white'}} onClick={  setPath("/home/ray/workspace/test10/virtuoso-documentaion"), toggleDrawer('right', true) }>Open Virtuoso</button>

        </p>
        <p>
        <button id="ray2" style={{color:'white'}} onClick={  setPath("/home/ray/workspace/test12/gitamine"),toggleDrawer('right', true) }>Open Gitamine</button>

        </p>
        <Drawer anchor="right" open={state.right} onClose={toggleDrawer('right', false)}>
           {sideList('right')}
        </Drawer>
    </div>
  );
}
