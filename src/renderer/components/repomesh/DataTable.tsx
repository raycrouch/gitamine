import * as React from "react"
import RepoSlideout from "./Repo-slideout";


export default class Table extends React.Component {
   constructor(props) {
      super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
      this.state = { //state is by default an object
         students: [
            { id: 1, name: 'Wasif', age: 21, email: 'wasif@email.com',url: '/home/ray/workspace/test10/virtuoso-documentaion' },
            { id: 2, name: 'Ali', age: 19, email: 'ali@email.com',url: '/home/ray/workspace/test12/gitamine' },
            { id: 3, name: 'Saad', age: 16, email: 'saad@email.com',url: '/home/ray/workspace/test10/virtuoso-documentaion' },
            { id: 4, name: 'Asad', age: 25, email: 'asad@email.com' ,url: '/home/ray/workspace/test12/gitamine'}
         ]
      }

   }


   click = (url:string) => {
         if(url) {
           console.log("THE UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUURL :" url)
           this.props.parentMethod(url);
        }
       }



   renderTableData() {
         return this.state.students.map((student, index) => {
            const { id, name, age, email,url } = student //destructuring
            return (
               <tr key={id}>
                  <td>{id}</td>
                  <td><a  href="#" onClick={() => this.click({url})}>{name}</a></td>
                  <td>{age}</td>
                  <td>{email}</td>
               </tr>
            )
         })
      }

      renderTableHeader() {
            let header = Object.keys(this.state.students[0])
            return header.map((key, index) => {
              if (key != "url") {
               return <th key={index}>{key.toUpperCase()}</th>
             }
            })
         }





         render() {
            return (
               <div>
                  <h1 id='title'>React Dynamic Table</h1>
                  <table id='students'>
                     <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.renderTableData()}
                     </tbody>
                  </table>
               </div>
            )
         }

}

 //exporting a component make it reusable and this is the beauty of react
