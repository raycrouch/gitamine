

//import './index.css';
import DropdownButton from 'react-bootstrap/DropdownButton'
import * as  Dropdown from 'react-bootstrap/Dropdown';
import { remote } from 'electron';
import * as React from 'react'
import { ThemeManager } from '../../../shared/theme-manager';

export default class DropdownButtonComp extends React.Component {

  themeManager: ThemeManager;



  async updateTheme(name?: string) {
    await this.themeManager.loadTheme(name);
    this.themeManager.updateCssVariables();
    this.setState({toggleDrawer(side, false)
      editorTheme: this.themeManager.getEditorTheme()
    });
  }

  componentDidMount() {
    this.updateTheme();
    }

  constructor(props: {}) {
    super(props);
    this.themeManager = new ThemeManager();
    this.state = {

    };
    this.updateTheme = this.updateTheme.bind(this);

  }


render() {

  return (

    <Dropdown>
      <Dropdown.Toggle variant="success" id="dropdown-basic">
        Dropdown Button
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>

)


}

}
