
import * as React from 'react';
import  Select from 'react-select'
var fs = window.require('fs');



 class FavouriteWorkspaces extends React.Component {
    constructor(props) {
      console.log("FavouriteWorkspaces props", props);
       super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
      // props.folderName = ''

       this.state = { //state is by default an object
            selectedOption: this.props.list[0]
       }
    }

   render()
   {
     const customStyles = {
       menu: (provided, state) => ({
         ...provided,
         width: '400px',
         borderBottom: '1px dotted darkgray',
         color: 'black',
         fontWeight: 'bold',
         padding: 10
       })

     }

     const handleChange = (selectedOption) => {
           this.setState({selectedOption})
           this.props.onSelect(selectedOption.value);
     }

    return (
      <Select
         name='folderNameSelectionId'
         styles={customStyles}
         options = {this.props.list}
         onChange={handleChange}
         value={this.state.selectedOption}
       />
      )
    }
}
export default FavouriteWorkspaces
