import * as React from 'react';

class Popup extends React.Component {
  constructor(props) {
    super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
    this.state = { //state is by default an object
         }
  }

  render() {
    return (
      <div className='popup'>
      <div className='popupinner'>
      <h1>{this.props.text}</h1>
      <p> This is just a standard popup </p>
      <button onClick={this.props.closePopup}>close me</button>
      </div>
      </div>
      );
    }
}

export default Popup;
