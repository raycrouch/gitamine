import * as React from 'react';

class PopupAddWorkspace extends React.Component {
  constructor(props) {
    super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
    this.state = { //state is by default an object
         }
  }

  render() {
    return (
      <div className='popup'>
      <div className='popupinner'>
      <h1>{this.props.text}</h1>
      <div> <input type="text" width='50px' /> </div>
      <button onClick={this.props.closeWorkspacePopup}>close me</button>
      </div>
      </div>
      );
    }
}

export default PopupAddWorkspace;
