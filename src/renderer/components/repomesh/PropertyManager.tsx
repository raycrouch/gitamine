import React, { Component } from 'react';
const fs = window.require('fs');

var RESTART_REQUIRED = false;
var SELECTED_DIRS = [];
var SELECTED_REPOS = [] ;  // for new repos available in stash
var FAVOURITE_WORKSPACES = [] ;   //a list of favourite workspaces
var SELECTED_WORKSPACE='';
var SELECTED_REPO='';
var FOLDER_NAMES = [];
var FULL_PATHS = [];
var GIT_STATUS_VALUE=[]; //holds the vaalue returned from git status check
var GIT_STATUS_HAS_CHANGED = false;
var BITBUCKET_USER_LOGIN = '';
var BITBUCKET_PASSWORD = '';
var BITBUCKET_API_BASE_URL = '';
var BITBUCKET_API_REPO_LOCATIONS = [];
var BITBUCKET_CLONE_BASE_PROTOCOL = '';
var BITBUCKET_CLONE_BASE_URL = '';
var BITBUCKET_CLONE_REPO_LOCATIONS=[];
var FOLDER_HASH_VAL = '';
var LOGGING_LEVEL = "info";
var ATOM_EXE_PATH = '';
var WEBSTORM_EXE_PATH ='';
var ECLIPSE_EXE_PATH = '';
var PYTHON_EXE_PATH = '';
var GITEXTENSIONS_EXE_PATH ='';
var SHOW_GIT_TIMER  ; //true or false
var MARKED_FOR_REMOVAL=[]; //transient vaable to hold values to be removed

// git status changed listener variables
var GIT_CHANGE_FILE_TYPES = [];
var GIT_CHANGE_EXCLUDE_FOLDERS = [];
var GIT_WATCHER_INTERVAL=3000;
var FOCUS_BRANCH_NAME = ''; // The name of the branch from the select element that has focus

// throttle variables to control status update - promises are very hungry and can chew out resources
var THROTTLE_CYCLE_GIT_LOCAL_STATUSES ='';
var THROTTLE_CYCLE_GIT_BRANCHES = '';
var THROTTLE_CYCLE_GIT_REMOTE_STATUSES ='';
var THROTTLE_CYCLE_WAIT_GIT_LOCAL_STATUSES ='';
var THROTTLE_CYCLE_WAIT_GIT_BRANCHES = '';
var THROTTLE_CYCLE_WAIT_GIT_REMOTE_STATUSES='';

var PROPERTIES_FILE_LOCATION= window.require("os").homedir() + '/.repomesh/properties/properties.json'

var loadAppProperties = function() {
    var rawdata = fs.readFileSync(PROPERTIES_FILE_LOCATION);
    var prop = JSON.parse(rawdata);

    SELECTED_WORKSPACE= prop.homeProjectPath;
    FAVOURITE_WORKSPACES = prop.favouriteWorkspaces.split(",");
    BITBUCKET_USER_LOGIN = prop.bitbucketUserLogin;
    BITBUCKET_PASSWORD = prop.bitbucketPassword;
    LOGGING_LEVEL = prop.loggingLevel;
    BITBUCKET_API_BASE_URL = prop.bitbucketAPIURL;
    BITBUCKET_API_REPO_LOCATIONS = prop.bitbucketAPIRepoLocations.split(",");
    BITBUCKET_CLONE_BASE_PROTOCOL = prop.bitbucketCloneBaseProtocol;
    BITBUCKET_CLONE_BASE_URL = prop.bitbucketCloneBaseUrl;
    BITBUCKET_CLONE_REPO_LOCATIONS = prop.bitbucketCloneRepoLocations.split(",");
    GIT_CHANGE_FILE_TYPES = prop.gitChangeFileTypes.split(",");
    GIT_CHANGE_EXCLUDE_FOLDERS = prop.gitChangeExcludeFolders.split(",");
    PYTHON_EXE_PATH = prop.pythonExePath
    GITEXTENSIONS_EXE_PATH = prop.gitExtensionsExePath;
    SHOW_GIT_TIMER = prop.showGitWatcherTimerDisplay;
    GIT_WATCHER_INTERVAL = prop.gitWatcherIntervalInSeconds;
    ATOM_EXE_PATH = prop.atomExePath;
    WEBSTORM_EXE_PATH  = prop.webStormExePath;
    ECLIPSE_EXE_PATH  = prop.eclipseExePath;
    THROTTLE_CYCLE_GIT_LOCAL_STATUSES =prop.throttleCycleGitLocalStatuses;
    THROTTLE_CYCLE_GIT_BRANCHES = prop.throttleCycleGitBranches;
    THROTTLE_CYCLE_GIT_REMOTE_STATUSES =prop.throttleCycleGitRemoveStatuses;
    THROTTLE_CYCLE_WAIT_GIT_LOCAL_STATUSES =prop.throttleCycleWaitGitLocalStatuses;
    THROTTLE_CYCLE_WAIT_GIT_BRANCHES = prop.throttleCycleWaitGitBranches;
    THROTTLE_CYCLE_WAIT_GIT_REMOTE_STATUSES=prop.throttleCycleWaitGitRemoteStatuses;

}

function getFavouriteWorkspacesArray() {
  loadAppProperties()
  var wrkspaces = []

  for(var i=0;i<FAVOURITE_WORKSPACES.length; i++) {
   wrkspaces.push({'value': FAVOURITE_WORKSPACES[i], 'label': FAVOURITE_WORKSPACES[i]})

  }


 return wrkspaces
}

class PropertyManager extends Component {
  constructor(props) {
     super(props) //since we are extending class Table so we have to use super in order to override Component class constructor


     const data = loadAppProperties();
     console.log(data)
     this.state = { //state is by default an object
      propertyManager:data
     }
  }





}
export default PropertyManager;
export {getFavouriteWorkspacesArray}
