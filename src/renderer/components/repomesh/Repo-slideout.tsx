//import { remote } from 'electron';
import * as ReactDOM from 'react-dom';
import * as React from 'react';
import  { useState } from 'react';
import { render } from 'react-dom';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import { App } from '../app';
import Workspace  from "./Workspace"
import { getFavouriteWorkspacesArray } from './PropertyManager'
import FavouriteWorkspaces from "./FavouriteWorkspaces"
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Popup from './Popup';
import PopupAddWorkspace from './PopupAddWorkspace';
import ErrorBoundary from './ErrorBoundary';

var pathStr = "";

const useStyles = makeStyles({
  list: {
    width: window.innerWidth-20,
  },
  fullList: {
    width: 'auto',
  },
});

var classes ;
var workspaces ;


export function SliderDrawer() {
  classes = useStyles();
  workspaces = getFavouriteWorkspacesArray();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
    selectedWorkspace: !!workspaces.length ? workspaces[0].value : '',
    workspaceList: workspaces,
  });

  const workspaceSel=(selWorkspace) =>{
    //workspaces = getFavouriteWorkspacesArray();
    setState({
      showPopup: false,
      showWorkspacePopup: false,
      selectedWorkspace: selWorkspace,
      workspaceList: workspaces
    });

  }

  type DrawerSide = 'top' | 'left' | 'bottom' | 'right';

  const toggleDrawer = (side: DrawerSide, open: boolean, url: string) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
  if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {

      return;
    }
    setState({ ...state, [side]: open });
  };

   const showSlideoutAndDisplayRepo=(url:string) => {
     pathStr = url;
     console.log("Called showSlideoutAndDisplayRepo:" + pathStr)
     setState({ ...state, 'right': true });

 }


 const setPath = (selectedPathStr) =>  {
    pathStr = selectedPathStr;
 }

  const setAppRef=(  app:App ) => {

  if (pathStr) {
    if (app ) {
         app.openOpenRepo(pathStr);
      }
    }
  }
[]
 // includes the cose button which floats on the open window
  const sideList = (side: DrawerSide) => (
  <div
      className={classes.list}
      role="presentation"
      style={{backgroundColor:'#333333', height:'100%',color:'#FFFFFF'}} >
      <div style={{float:'left',height:'40px'}}>
         <button onClick={toggleDrawer('right', false, '')} style={{color:'white',height:'40px',fontSize:'20px', position:'absolute', right:'50px',zIndex:"1000"}}> Close</button>
      </div>
      <App ref={setAppRef} />
  </div>
  );

  const toggleWorkspacePopup=() => {
     setState({
     showWorkspacePopup: !state.showWorkspacePopup
   });
  }

  const togglePopup=() => {
     setState({
     showPopup: !state.showPopup
   });
  }

  const whoAmI = () => {
    return "I am the Repo-slidout object"
  }

  const addWorkspaceAction =() =>{
    toggleWorkspacePopup()
  }

  const settingsAction =() =>{
    togglePopup()
  }


  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
      setAnchorEl(event.currentTarget);
      console.log("currentTarget: " + event.currentTarget.id)
      if (event.currentTarget.id === "add-workspace-id") {
        addWorkspaceAction()
      }
      if (event.currentTarget.id === "settings-id") {
        settingsAction()
      }
  };

  const handleClose = () => {
      console.log("closed it")
      setState({
        workspaceList: getFavouriteWorkspacesArray()
      });
      setAnchorEl(null);
  };

  return (
    <div style={{marginLeft:'20px', marginRight:'20p'}}>

     <span style={{ width:'100%',marginTop:'40px', marginBottom:'10px',float:'left' }}>
      <div style={{ width:'60%', float:'left' }}>
         <FavouriteWorkspaces list={state.workspaceList} onSelect={(val)=> workspaceSel(val)}/>
      </div>

       <div style={{ textAlign:'right'; width:'35%', float:'right' , marginRight:'40px'}}>
           <Button
           style={{
             borderRadius: 8,
             backgroundColor: "#62bd7f",
             padding: "8px 30px 8px 30px",
             fontSize: "16px",
             color: "white",
             height: "40px"
             }}
              variant="contained"
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={handleClick}>
              Action
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem id="add-workspace-id" onClick={handleClick}>Add Workspace</MenuItem>
              <MenuItem id="remove-workspace-id" onClick={handleClose}>Remove Workspace</MenuItem>
              <MenuItem id="create-branch-id" onClick={handleClose}>Create Branch</MenuItem>
              <MenuItem id="switch-branch-id" onClick={handleClose}>Switch Branch</MenuItem>
              <MenuItem id="remove-folder-id" onClick={handleClose}>Remove Folder</MenuItem>
              <MenuItem id="settings-id" onClick={handleClick}>Settings</MenuItem>
              <MenuItem id="logout-id" onClick={handleClose}>Logout</MenuItem>
            </Menu>

     </div>
      </span>

      {state.showWorkspacePopup ?
      <PopupAddWorkspace
                //see https://minutemailer.github.io/react-popup/
                text='Click "Close Button" to hide popup'
                closePopup={toggleWorkspacePopup.bind(this)}
      />

      : null
      }

      {state.showPopup ?
      <Popup
                //see https://minutemailer.github.io/react-popup/
                text='Click "Close Button" to hide popup'
                closePopup={togglePopup.bind(this)}
      />

      : null
      }

      <div style={{width:'98%',marginTop:'20px'}}>
        <Workspace folderName={state.selectedWorkspace} parentMethod={showSlideoutAndDisplayRepo} />
      </div>
      <ErrorBoundary>
        <Drawer anchor="right" open={state.right} onClose={toggleDrawer('right', false, '')}>
           {sideList('right')}
        </Drawer>
      </ErrorBoundary>
   </div>
  );

}
