import * as React from 'react';
import * as Git from 'nodegit';

const diffOptions = {
  flags: Git.Diff.OPTION.INCLUDE_UNTRACKED |
  Git.Diff.OPTION.RECURSE_UNTRACKED_DIRS
}


export class StatusBadgeLocal extends React.Component {
  constructor(props) {
    super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
    //console.log("props.folderName >>>>>>> ", props.repoFolder);

    this.state = { //state is by default an object
       repoStatus:'',
       repo:''
    }

 }

  async  getGitStatus(repoPathStr:string) {
     var pathToGitFolder = repoPathStr +"/.git"

     try {
       var pathToRepo = window.require("path").resolve(pathToGitFolder)
       this.setState({
         repo: pathToRepo
       });

          var status= ''
          var  repo = await (Git.Repository.open(pathToRepo))
          var statuses = await repo.getStatus());


          if (!statuses) {
               this.setState({
               repoStatus: -1
             });
          }
          else {
            this.setState({
            repoStatus: statuses.length
          });
          }


      }
      catch (err) {

             this.setState({
                repoStatus: -1
           });
      }
      return;
  }


 componentDidMount() {
   var repoLocation = this.props.repoFolder + "/" + this.props.repoName
   this.getGitStatus(repoLocation)
 }

componentDidUpdate(prevProps) {
  let newRepoPath = this.props.repoFolder + "/" + this.props.repoName;
  if (prevProps.repoName !== this.props.repoName)
    this.getGitStatus(newRepoPath);
}

render() {

  var that = this;
      {

               if (that.state.repoStatus == 0) {
                  return  <img src="../images/greentick.png" height='30' width='30'/>
               }
               else if( that.state.repoStatus > 0) {
                return  <div   style={{marginLeft:'13px',width:'40px',height:'40px',border:'1px solid orange',color:'orange',paddingLeft:'1px', paddingRight:'1px', cursor: 'pointer'}}> <div style={{marginTop:'-15',paddingTop:'2px',fontSize:'2'}}> <font size='2' style ={{fontWeight:'normal'}}> &#9754;&#916;&#9755; </font></div><div style={{marginTop:'-4px'}}  >{that.state.repoStatus} </div></div>
               }
               else {
                  return  <img src="../images/notgit.png" height='30' width='30'/>
              }
      }
  }
}
