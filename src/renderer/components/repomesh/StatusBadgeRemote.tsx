import * as React from 'react';
import * as Git from 'nodegit';
import { execSync } from 'child_process'



export class StatusBadgeRemote extends React.Component {
  constructor(props) {
    super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
    this.state = { //state is by default an object
       repoStatus:'',
       repo:''
         }

 }

  async  getGitStatus(repoPathStr:string) {
     var pathToGitFolder = repoPathStr +"/.git"
     try {
       var pathToRepo = window.require("path").resolve(pathToGitFolder)
       this.setState({
         repo: pathToRepo
       });

          var status= ''
          var  repo = await (Git.Repository.open(pathToRepo))
          if(repo) {
             var headCommit = await repo.getHeadCommit()
             //console.log("HEAD   COMMIT " + headCommit)
             var currentBranch = await repo.getCurrentBranch()
            // console.log(repoPathStr)
            //execSync( "git remote update", {cwd:repoPathStr})
             var localCommit  = execSync( "git status --branch --porcelain", {cwd:repoPathStr})
            // console.log(localCommit)

             var  behindStr = new TextDecoder("utf-8").decode(localCommit).trim();
             //console.log("REMOTE COMMIT " + behindStr )

            var behindCount =''
            if (behindStr) {
               var pos = behindStr.indexOf("[behind ")
               if (pos > -1) {
                  var foundStr = behindStr.substring(pos + 8)
                  var  strAr   = foundStr.split(']')
                  behindCount  = strAr[0]
               }
               else{
                  behindCount = 0
               }
            }
          }
          if (behindCount >= 0) {
               this.setState({
               repoStatus: behindCount
             });
          }
          else {
            this.setState({
            repoStatus: -1
          });
          }
      }
      catch (err) {

             this.setState({
                repoStatus: -1
           });
      }
      return;
  }

 componentDidMount() {
   var repoLocation = this.props.repoFolder + "/" + this.props.repoName
   this.getGitStatus(repoLocation)
 }

 componentDidUpdate(prevProps) {
   let newRepoPath = this.props.repoFolder + "/" + this.props.repoName;
   if (prevProps.repoName !== this.props.repoName)
     this.getGitStatus(newRepoPath);
 }

render() {
  var that = this;
      {
             if (that.state.repoStatus == 0) {
                return  <img src="../images/greentick.png" height='30' width='30'/>
             }
             else if( that.state.repoStatus > 0) {
                return  <div   style={{marginLeft:'13px',width:'40px',height:'40px',border:'1px solid orange', color:'orange',paddingLeft:'1px', paddingRight:'1px', cursor: 'pointer' }}> <div  style={{marginTop:'-15'}}><font size='1' >behind </font></div><div style={{ marginTop:'-4px' }}  > {that.state.repoStatus} </div></div>
             }
             else {
                return  <div />
            }
      }

  }
}
