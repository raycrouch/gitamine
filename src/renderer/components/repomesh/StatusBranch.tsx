import * as React from 'react';
import * as Git from 'nodegit';



export class StatusBranch extends React.Component {
  constructor(props) {
    super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
    this.state = { //state is by default an object
       repoBranch:'',
       repo:''
         }

   }

  async  getGitStatus(repoPathStr:string) {
     this.setState({
       repoBranch: ''
     });
       var pathToGitFolder = repoPathStr +"/.git"
    try {
       var pathToRepo = window.require("path").resolve(pathToGitFolder)
       this.setState({
         repo: pathToRepo
       });

          var  repo = await (Git.Repository.open(pathToRepo))
          if (repo) {
             var headCommit = await repo.getHeadCommit()
             //console.log("HEAD   COMMIT " + headCommit)
             var currentBranch = await repo.getCurrentBranch()

             console.log("CURRENT BRANCH: " + currentBranch  )
             if (currentBranch) {
               this.setState({
                 repoBranch: currentBranch
              });
             }
            else {
              this.setState({
              repoBranch: ''
            });
            }
          }
        }
      catch (err) {

             this.setState({
                repoBranch: ''
           });
      }
      return;
  }


 componentDidMount() {
   var repoLocation = this.props.repoFolder + "/" + this.props.repoName
   this.getGitStatus(repoLocation)
 }

 componentDidUpdate(prevProps) {
   let newRepoPath = this.props.repoFolder + "/" + this.props.repoName;
   if (prevProps.repoName !== this.props.repoName) {
     console.log("getting status for repo :" + newRepoPath)
     this.getGitStatus(newRepoPath);
   }

 }

render() {
  var that = this;
      {
             if (that.state.repoBranch) {
                var branch:string =   that.state.repoBranch.toString()
                var searchStr = "refs/heads/"
                var pos = branch.indexOf(searchStr);
                var branchNameStr = branch.substring(pos + searchStr.length);
                return  <div>{branchNameStr}</div>
             }
             else {
                return  <div />
            }
      }

  }
}
