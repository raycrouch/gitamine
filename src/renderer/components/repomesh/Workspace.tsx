
import * as React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import  IconButton  from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import { StatusBadgeLocal } from './StatusBadgeLocal';
import { StatusBadgeRemote } from './StatusBadgeRemote';
import { StatusBranch } from './StatusBranch';
import Alert from '@material-ui/lab/Alert'

const styles = {
  button: {
    margin: 8,
  },
};

var fs = window.require('fs');
var Git = window.require('nodegit')


function getBranch(folderName) {
  return "master"
}

function getLocalStatus(folderName) {
  return "local"
}

function getRemoteStatus(folderName) {
  return "remote"
}

function runCheck(folderName) {
  if (folderName) {
      console.log("Click function detail to be set:" + folderName)
  }
  return
}

function runHeaderCheck() {
  console.log("Click function Header process to be set:" )
}



 class Workspace extends React.Component {

    constructor(props: {}) {
       super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
       console.log("workspace props: ", props);
       console.log("SELECTED FOLDER NAME : ", this.props.folderName);
       const data = this.getFileNameList(   this.props.folderName);
       this.state = { //state is by default an object
          tableData: '',
          workspace: data,
          appId: '',
          snackbaropen: false,
          snackbarmsg: ''
       }
    }

    componentDidUpdate(prevProps) {
  // Typical usage (don't forget to compare props):
    if (this.props.folderName && this.props.folderName !== prevProps.folderName) {
      const data = this.getFileNameList(  this.props.folderName);
      this.setState({
           workspace: data
      });
    }
}
 //getFileNameList("/home/ray")

 async doStuff(localRepoAddress){
    if (fs.existsSync(localRepoAddress +"/.git")) {
    // Do something
      var testValue="BLAHHHHHHH"
      this.setState({
        raysTestValue:testValue
      })
      this.props.parentMethod(localRepoAddress)
      }
      else {
         this.setState({snackbaropen:true, snackbarmsg:localRepoAddress + " is Not a Git repository"})
         console.log("Not a valid git repository: " + localRepoAddress)
      }
 }

 snackbarClose=(event) => {
  this.setState({snackbaropen:false})
 }

 click = (url:string) => {
    var localRepoAddress = this.props.folderName + "/" + url.name
    var pathToGitFolder = localRepoAddress  +"/.git"
    var pathToRepo = require("path").resolve(pathToGitFolder)
    var theParentMethod =  this.props.parentMethod

    var promise = new Promise( (resolve, reject) => {
        let repo = Git.Repository.open(pathToRepo)
        if (repo) {
           resolve("Promise resolved successfully:" + JSON.stringify(repo));
        }
        else {
           reject(Error("Promise rejected"));
        }
     });

     var that = this;
     promise.then((result) {
      //  console.log("RRRRRRRRRRRRR: " +result + " localRepoAddress:" + localRepoAddress); // "Promise resolved successfully"
          console.log(that.props.folderName + "result: " + result)
          that.doStuff(localRepoAddress)
     }), err => {
        console.log(err); // Error: "Promise rejected"
     };
 }


getFileNameList( folderName) {
   var fileNamesList = []
   var filenames= fs.readdirSync(folderName);
   for (var i= 0;i<filenames.length;i++) {
       var curFilename = filenames[i]
       var fullFileNameAndPath = folderName + "/" + curFilename
       if (fs.existsSync(fullFileNameAndPath) && fs.lstatSync(fullFileNameAndPath).isDirectory()) {
         var record ={" ":"","key":i,"name": curFilename, "branch": getBranch(curFilename), "local": getLocalStatus(curFilename), "remote" : getRemoteStatus(curFilename)}
         fileNamesList.push(record)
       }
    }
     //console.log(filename)
    return fileNamesList

};


 renderTableData() {
   return this.state.workspace.map((workfolder, index) => {
      const {name,branch,local,remote } = workfolder //destructuring

      return (
          <tr key={`thing-${index}`}><td style ={{'textAlign':'center'}}><Checkbox
                  value="checked_{name}.name"
                  onChange={()=>runCheck({name}.name)}
                  iconstyle={{fill: 'white'}}
                  style={{color:'white'}}
                  /></td>
            <td><a  href="#" onClick={() => this.click({name})}>{name}</a></td>
            <td><StatusBranch repoName={{name}.name} repoFolder={this.props.folderName} /></td>
            <td style={{width:'70px',textAlign:'center'}}><StatusBadgeLocal repoName={{name}.name} repoFolder={this.props.folderName} /></td>
            <td style={{width:'70px',textAlign:'center'}}><StatusBadgeRemote repoName={{name}.name} repoFolder={this.props.folderName} /></td></tr>
      )
   })
}

renderTableHeader() {
    let header = Object.keys(this.state.workspace[0])
    return header.map((key,value) => {
      if(JSON.stringify({value}.value)=="0"){
          return <th key={`header-${value}`} style={{'textAlign':'center'}} ><Checkbox
          value='checked_header'
          iconstyle={{fill: 'white'}}
          style={{color:'white'}}
          onChange={()=>runHeaderCheck()}
           /></th>
      }
      else if (key.toUpperCase() === "KEY") {
        //ignore
      }
       else{
          return <th key={key}>{key.toUpperCase()}</th>
      }
    })
}

render() {

    return (

      <div>
      <Snackbar

         anchorOrigin={{  horizontal:'center', vertical:'top' }}
         open={ this.state.snackbaropen }
         autoHideDuration={ 4000 }
         onClose={ this.snackbarClose }
         action={[
           <IconButton
           key="close"
           arial-label="Close"
           color="black"
           onClick={this.snackbarClose}
           > x
           </IconButton>
         ]}

      >
       <Alert severity="error">
       {this.state.snackbarmsg}
       </Alert>
      </Snackbar>
      <table id='workspace'>
           <thead>
              <tr>
                <th key={0} colSpan={3}>REPOSITORIES</th>
                <th key={1} colSpan={2}>STATUS</th>
              </tr>
              <tr>
                {this.renderTableHeader()}
              </tr>
            </thead>
            <tbody>
               {this.renderTableData()}
           </tbody>
        </table>
     </div>
    )
   }
}

 export default Workspace
