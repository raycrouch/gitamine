//import { remote } from 'electron';
import * as React from 'react';
import * as Git from 'nodegit';
import { App } from './app';
import * as ReactDOM from 'react-dom';

let app: App;

let currentPath = "";

function setAppRef(  a:App  ) {
  app = a;
  //pathStr = path;
  console.log(currentPath);

  app.openOpenRepo(currentPath);
}

function renderMe(path:string) {
  console.log("called renderMe- path:" + path);
  currentPath = path;
  ReactDOM.render(<App ref={setAppRef} />, document.getElementById('container'));

}



export class NewComponent extends React.Component {
  constructor() {
    super();

  render() {
    return (
      <div {...this.props}>
        new component
      </div>
    );
  }
}
}


export class App extends React.Component {
  constructor() {
    super();

    this.state = {
      clicked: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    console.log("clicked it");
    this.setState({

      clicked: true
    });
    //var pathStr = "/home/ray/workspace/test12/gitamine"
    var pathStr = "/home/ray/workspace/test10/virtuoso-documentaion"
    renderMe(pathStr)
  }

  render() {
    return (
      <div>
      <p> Got here </p>
        <button onClick={this.handleClick }>
        Click me
        </button>
      </div>
    );
  }
};
