import { ipcRenderer } from 'electron';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as unhandled from 'electron-unhandled';
import { RepoMain } from './components/repomesh/Repo-main';
import { App } from './app';

let repoMain: RepoMain;

function setRepoRef(a: RepoMain) {
  repoMain = a;
}

let app: App;

function setAppRef(a: App) {
  app = a;
}



function render() {
  ReactDOM.render(<RepoMain ref={setRepoRef} />, document.getElementById('container'));

}

render();

// Events
/*
if (app) {

console.log("Running app form index.ts: '/home/ray/workspace/test12/gitamine'"  )
app.openOpenRepo('/home/ray/workspace/test12/gitamine')
}
*/
ipcRenderer.on('clone-repo', () => {
  if (app) {
    app.openCloneRepoDialog();
  }
});

ipcRenderer.on('init-repo', () => {
  if (app) {
    app.openInitRepoDialog();
  }
});

ipcRenderer.on('open-repo', () => {
  if (app) {
    //app.openOpenRepoDialog();
    app.openOpenRepo('/home/ray/workspace/test12/gitamine')
  }
});

ipcRenderer.on('preferences', () => {
  if (app) {
    app.openPreferencesDialog();
  }
});

// Unhandled exceptions

function handleError(e: any) {
  // This was necessary for node-watch, is it still necessary for chokidar?
  if (e.code === 'ENOTDIR' || e.code === 'ENOENT') {
    console.log("error: " + e);
  } else {
    console.error(e);
  }
}

unhandled({
  logger: handleError
});
